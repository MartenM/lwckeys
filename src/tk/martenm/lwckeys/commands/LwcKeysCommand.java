package tk.martenm.lwckeys.commands;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.martenm.lwckeys.MainClass;
import tk.martenm.lwckeys.helpers.PluginMessages;
import tk.martenm.lwckeys.objects.Key;

/**
 * Created by Marten on 18-8-2017.
 */
public class LwcKeysCommand implements CommandExecutor {

    private MainClass plugin;
    public LwcKeysCommand(MainClass plugin){
        this.plugin = plugin;
    }


    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!sender.hasPermission("lwckeys.admin")){
            sender.sendMessage(PluginMessages.NO_PERMISSION);
            return true;
        }


        if(args.length == 0){
            sendHelp(sender);
            return true;
        }

        if(args[0].equalsIgnoreCase("keys")){

            for(Key key : plugin.keys){
                sender.sendMessage("=====================");
                sender.sendMessage("KeyName: " + key.getName());
                sender.sendMessage("ConfigName: " + key.getConfigName());
                sender.sendMessage("Days: " + key.getDays());
                sender.sendMessage("Breakchance: " + key.getBreakChance());
                sender.sendMessage("LoseinvChance: " + key.getLoseInvChance());
            }

        }

        if(args[0].equalsIgnoreCase("givekey")){
            if(args.length < 4){
                sender.sendMessage(ChatColor.RED + "Wrong usage. Use /lwckeys givekey <configname> <target> <amount>");
                return true;
            }

            Key key = plugin.getKey(args[1]);
            if(key == null){
                sender.sendMessage(ChatColor.RED + "Key was not found!");
                return true;
            }

            Player target = plugin.getServer().getPlayer(args[2]);
            if(target == null){
                sender.sendMessage(ChatColor.RED + "Player was not found!");
                return true;
            }

            int amount = 1;
            try {
                amount = Integer.parseInt(args[3]);
            }
            catch (Exception ex) {

            }

            ItemStack keys = key.createKey();
            keys.setAmount(amount);

            target.getInventory().addItem(keys);
            sender.sendMessage(ChatColor.GREEN + "Keys added successfully!");
            return true;
        }
        return true;
    }

    private String formatCommand(String command, String desc){
        return ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + "/" + command + ChatColor.GRAY + "  -  " + ChatColor.WHITE + desc);
    }

    private void sendHelp(CommandSender sender){
        sender.sendMessage(ChatColor.GRAY + "============[ " + ChatColor.RED + "LWC Keys" + ChatColor.GRAY + "]============");
        sender.sendMessage(formatCommand("help", "Show this usefull help"));
        sender.sendMessage(formatCommand("givekey", "Give a user a key!"));
    }
}
