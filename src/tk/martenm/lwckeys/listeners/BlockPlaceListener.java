package tk.martenm.lwckeys.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import tk.martenm.lwckeys.MainClass;
import tk.martenm.lwckeys.objects.Key;

/**
 * Created by Marten on 28-8-2017.
 */
public class BlockPlaceListener implements Listener {

    private MainClass plugin;
    public BlockPlaceListener(MainClass plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onblockplaceevent(BlockPlaceEvent event){
        if(event.getPlayer() == null){
            return;
        }

        if(event.getItemInHand().getType() != Material.TRIPWIRE_HOOK){
            return;
        }

        for(Key key : plugin.keys){
            if(key.createKey().isSimilar(event.getItemInHand())) {
                event.setCancelled(true);
                return;
            }
        }
    }
}
