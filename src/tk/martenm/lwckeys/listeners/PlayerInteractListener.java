package tk.martenm.lwckeys.listeners;

import com.griefcraft.model.Protection;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import tk.martenm.lwckeys.MainClass;
import tk.martenm.lwckeys.helpers.PluginMessages;
import tk.martenm.lwckeys.helpers.PluginUtils;
import tk.martenm.lwckeys.objects.Key;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Marten on 28-8-2017.
 */
public class PlayerInteractListener implements Listener {

    private MainClass plugin;

    public PlayerInteractListener(MainClass plugin){
        this.plugin = plugin;
    }


    @EventHandler
    public void playerInteract(PlayerInteractEvent event){

        if(event.getClickedBlock() == null){
            return;
        }

        if(event.getClickedBlock().getType() == Material.AIR){
            return;
        }

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK){
            return;
        }

        if(event.getPlayer().getInventory().getItemInMainHand() == null){
            return;
        }

        if(event.getPlayer().getInventory().getItemInMainHand().getType() != Material.TRIPWIRE_HOOK){
            return;
        }

        Key key = plugin.getKey(event.getPlayer().getInventory().getItemInMainHand());
        if(key == null){
            return;
        }

        if(!event.getPlayer().getInventory().getItemInMainHand().isSimilar(key.createKey())){
            int amount =  event.getPlayer().getInventory().getItemInMainHand().getAmount();
            ItemStack stack = key.createKey();
            stack.setAmount(amount);
            event.getPlayer().getInventory().setItemInMainHand(stack);
            event.getPlayer().sendMessage(PluginMessages.KEY_CHANGED);
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_NOTE_HARP, 1, 2);
            return;
        }

        event.setCancelled(true);

        Protection protection = plugin.lwc.findProtection(event.getClickedBlock());

        if(protection == null){
            event.getPlayer().sendMessage(PluginMessages.NO_PROTECTION);
            return;
        }

        if(protection.isRealOwner(event.getPlayer())){
            event.getPlayer().sendMessage(PluginMessages.OWN_PROTECTION);
            return;
        }

        if(PluginUtils.getBlockInventory(event.getClickedBlock()) == null){
            event.getPlayer().sendMessage(ChatColor.RED + "Woops, this block does not have an inventory!");
            return;
        }

        if(key.isOnlyEmpty()){
            if(!PluginUtils.isEmpty(PluginUtils.getBlockInventory(event.getClickedBlock()))){
                event.getPlayer().sendMessage(PluginMessages.ONLY_EMPTY);
                return;
            }
        }

        //Player owner = protection.getBukkitOwner();
        String uuid = protection.getOwner();

        OfflinePlayer offline_Owner = plugin.getServer().getOfflinePlayer(UUID.fromString(uuid));

        long difference = System.currentTimeMillis() - offline_Owner.getLastPlayed();

        long seconds = TimeUnit.MILLISECONDS.toSeconds(difference);
        long days = seconds / 60 / 60 / 24;

        if(days < key.getDays()){
            event.getPlayer().sendMessage(PluginMessages.NOT_EXPIRED
                    .replace("%keydays%", key.getDays() + "")
                    .replace("%left%", (days - key.getDays()) * -1 + ""));
            //event.getPlayer().sendMessage(ChatColor.GREEN + "Failed to unlock the item.");
            return;
        }

        double randNum = Math.random();
        int chance = (int) (randNum * 100 + 1);

        if(key.getBreakChance() != 0) {
            if (chance < key.getBreakChance()) {
                //BREAKS
                event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount() - 1);
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_ANVIL_BREAK, 5, 1);
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_ANVIL_USE, 2, 2);
                event.getPlayer().sendMessage(PluginMessages.KEY_BROKE);
            }
        }

        //Start async remove task
        new BukkitRunnable(){

            @Override
            public void run() {

                //Start synced waiting animation
                BukkitTask task = new BukkitRunnable(){
                    int count;
                    @Override
                    public void run() {
                        if(count >= 10){
                            count = 0;
                        }

                        for(int i = 0; i < 20; i++) {
                            double radians = Math.toRadians(18 * i);
                            double x = Math.sin(radians) * 0.7;
                            double z = Math.cos(radians) * 0.7;

                            for(int d = 0; d < 5; d++)
                                event.getClickedBlock().getWorld().spawnParticle(Particle.REDSTONE, event.getClickedBlock().getX() + x + 0.5, event.getClickedBlock().getY() + count * 0.1, event.getClickedBlock().getZ() + z + 0.5, 0, 0.96, 0.6, 0, 1 );
                        }
                        count++;
                    }
                }.runTaskTimer(plugin, 0, 2);

                protection.remove();

                //cancel wait animation
                task.cancel();

                //Back to sync
                new BukkitRunnable(){
                    @Override
                    public void run() {
                        event.getPlayer().sendMessage(PluginMessages.SUCCESS_UNLOCK);
                        key.handleKeyUse(event, event.getPlayer());
                    }
                }.runTask(plugin);
            }
        }.runTaskAsynchronously(plugin);
    }


}
