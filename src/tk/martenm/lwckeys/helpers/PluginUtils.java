package tk.martenm.lwckeys.helpers;

import org.bukkit.block.*;
import org.bukkit.inventory.Inventory;
import tk.martenm.lwckeys.MainClass;

/**
 * Created by Marten on 29-8-2017.
 */
public class PluginUtils {

    public static Inventory getBlockInventory(Block block){

        if(block.getState() == null){
            return null;
        }

        if(block.getState() instanceof Chest){
            Chest c = (Chest) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof ShulkerBox){
            ShulkerBox c = (ShulkerBox) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof Dropper){
            Dropper c = (Dropper) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof Hopper){
            Hopper c = (Hopper) block.getState();
            return c.getInventory();
        }
        //
        if(block.getState() instanceof Beacon){
            Beacon c = (Beacon) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof Dispenser){
            Dispenser c = (Dispenser) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof Furnace){
            Furnace c = (Furnace) block.getState();
            return c.getInventory();
        }

        if(block.getState() instanceof BrewingStand){
            BrewingStand c = (BrewingStand) block.getState();
            return c.getInventory();
        }
        return null;
    }

    public static boolean isEmpty(Inventory inv){
        for(int i = 0; i < inv.getSize(); i++){
            if(inv.getItem(i) != null){
                return false;
            }
        }
        return true;
    }

    public static String getColor(MainClass plugin, int c){

        if(c >= 90){
            return plugin.getConfig().getString("key.colours.bad");
        } else if(c <= 10){
            return plugin.getConfig().getString("key.colours.good");
        } else{
            return plugin.getConfig().getString("key.colours.medium");
        }
    }
}
