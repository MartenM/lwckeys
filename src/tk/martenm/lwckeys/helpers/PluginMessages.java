package tk.martenm.lwckeys.helpers;

import net.md_5.bungee.api.ChatColor;
import tk.martenm.lwckeys.MainClass;

/**
 * Created by Marten on 18-8-2017.
 */
public class PluginMessages {

    private static MainClass plugin;

    private PluginMessages(MainClass plugin) {
        this.plugin = plugin;
    }

    public static void init(MainClass p) {
        plugin = p;

        NO_PERMISSION = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.no permission"));
        KEY_DISPLAYNAME = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("key.name"));
        OWN_PROTECTION = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.own"));
        NOT_EXPIRED = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.not expired"));
        KEY_BROKE = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.key broke"));
        NO_PROTECTION = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.no protection"));
        INV_LOST = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.inv lost"));
        ONLY_EMPTY = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.only empty"));
        KEY_CHANGED = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.key changed"));
        SUCCESS_UNLOCK = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.success unlock"));
    }

    public static String NO_PERMISSION;

    public static String KEY_DISPLAYNAME;

    public static String KEY_CHANGED;

    public static String OWN_PROTECTION;

    public static String NOT_EXPIRED;

    public static String KEY_BROKE;

    public static String INV_LOST;

    public static String NO_PROTECTION;

    public static String ONLY_EMPTY;

    public static String SUCCESS_UNLOCK;
}
