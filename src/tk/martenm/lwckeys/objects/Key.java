package tk.martenm.lwckeys.objects;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import tk.martenm.lwckeys.MainClass;
import tk.martenm.lwckeys.helpers.ItemUtil;
import tk.martenm.lwckeys.helpers.PluginMessages;
import tk.martenm.lwckeys.helpers.PluginUtils;

import java.util.List;

/**
 * Created by Marten on 18-8-2017.
 */
public class Key {

    protected MainClass plugin;
    private int days;
    private String name;
    private int loseInvChance;
    private String configName;
    private int breakChance;
    private boolean onlyEmpty;

    public Key(MainClass plugin, String configName, String name, int days, int loseInvChance, int breakChance, boolean onlyEmpty)
    {
        this.plugin = plugin;
        this.name = name;
        this.days = days;
        this.loseInvChance = loseInvChance;
        this.configName = configName;
        this.breakChance = breakChance;
        this.onlyEmpty = onlyEmpty;
    }

    public ItemStack createKey(){
        ItemStack key = new ItemStack(Material.TRIPWIRE_HOOK);
        ItemMeta meta = key.getItemMeta();

        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', PluginMessages.KEY_DISPLAYNAME.replace("%name%", getName())));
        meta.setLocalizedName(ChatColor.translateAlternateColorCodes('&', PluginMessages.KEY_DISPLAYNAME.replace("%name%", getName())));
        List<String> lore;

        lore = plugin.getConfig().getStringList("key.lore");
        for(int i = 0; i < lore.size(); i++){
            lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)
                    .replace("%days%", getDays() + "")
                    .replace("%loseinv%", PluginUtils.getColor(plugin, loseInvChance) + loseInvChance)
                    .replace("%keybreak%", PluginUtils.getColor(plugin, breakChance) + breakChance)));
        }

        if(onlyEmpty){
            lore.add(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("key.only empty")));
        }

        meta.setLore(lore);

        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.setUnbreakable(true);
        meta.addEnchant(Enchantment.LUCK, 1, true);

        key.setItemMeta(meta);

        key = ItemUtil.setTag(key, "lwckey", getConfigName());

        return key;
    }

    public int getDays() {
        return days;
    }

    public String getName() {
        return name;
    }

    public String getConfigName() {
        return configName;
    }

    public int getLoseInvChance(){
        return this.loseInvChance;
    }

    public int getBreakChance(){
        return breakChance;
    }

    public void handleKeyUse(PlayerInteractEvent event, Player player){

        //animate
        player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, event.getClickedBlock().getLocation(), 6, 1, 1, 1);
        player.getWorld().playSound(event.getClickedBlock().getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 2, 0.5f);

        double randNum = Math.random();
        int chance = (int) (randNum * 100 + 1);

        new BukkitRunnable(){
            int count = 0;
            @Override
            public void run() {
                if(count >= 10){
                    this.cancel();
                    return;
                }

                for(int i = 0; i < 20; i++) {
                    double radians = Math.toRadians(18 * i);
                    double x = Math.sin(radians) * 0.7;
                    double z = Math.cos(radians) * 0.7;

                    for(int d = 0; d < 5; d++)
                    event.getClickedBlock().getWorld().spawnParticle(Particle.REDSTONE, event.getClickedBlock().getX() + x + 0.5, event.getClickedBlock().getY() + count * 0.1, event.getClickedBlock().getZ() + z + 0.5, 0, 0.001, 1, 0.02, 1 );
                }
                count++;
            }
        }.runTaskTimer(plugin, 0, 2);

        chance = (int) (Math.random() * 100 + 1);

        if(loseInvChance != 0){
            if(chance < loseInvChance) {
                player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 4, 0.5f);
                player.sendMessage(PluginMessages.INV_LOST);
                Inventory inv = PluginUtils.getBlockInventory(event.getClickedBlock());
                inv.clear();

                for(int i = 0; i < 5; i++){
                    player.getWorld().spawnParticle(Particle.LAVA, event.getClickedBlock().getLocation(), 30, 1, 1, 1, 0.001);
                }
            }
        }
    }

    public boolean isOnlyEmpty() {
        return onlyEmpty;
    }
}
