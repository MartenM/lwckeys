package tk.martenm.lwckeys;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import tk.martenm.lwckeys.commands.LwcKeysCommand;
import tk.martenm.lwckeys.helpers.ItemUtil;
import tk.martenm.lwckeys.helpers.PluginMessages;
import tk.martenm.lwckeys.listeners.BlockPlaceListener;
import tk.martenm.lwckeys.listeners.PlayerInteractListener;
import tk.martenm.lwckeys.objects.Key;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Marten on 18-8-2017.
 */
public class MainClass extends JavaPlugin {

    private final Logger logger = getLogger();

    public LWC lwc;
    public List<Key> keys = new ArrayList<>();

    public void onEnable(){
        logger.info("Enabling the plugin...");

        if(!getServer().getPluginManager().isPluginEnabled("LWC")){
            logger.warning("Could not find the LWC plugin that is needed! Disabling!");
            getPluginLoader().disablePlugin(this);
            return;
        }

        lwc = ((LWCPlugin) Bukkit.getPluginManager().getPlugin("LWC")).getLWC();

        registerConfig();
        registerCommands();
        registerEvents();

        PluginMessages.init(this);

        createKeys();

        logger.info("Successfully enabled the plugin");
    }

    public void onDisable(){
        logger.info("Disabling the plugin!");

        logger.info("Successfully disabled the plugin!");
    }


    private void registerConfig(){
        saveDefaultConfig();
    }

    private void registerCommands(){
        getCommand("lwckeys").setExecutor(new LwcKeysCommand(this));
    }

    private void registerEvents(){
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new BlockPlaceListener(this), this);
        pm.registerEvents(new PlayerInteractListener(this), this);
    }

    private void createKeys(){
        for(String name : getConfig().getConfigurationSection("keys").getKeys(false)){
            int duration = getConfig().getInt("keys." + name + ".days");
            String display = getConfig().getString("keys." + name + ".display");
            int breakChance = getConfig().getInt("keys." + name + ".breakchance");
            int loseInvChance = getConfig().getInt("keys." + name + ".loseinvchance");
            boolean onlyEmpty = getConfig().getBoolean("keys." + name + ".only empty");

            Key key = new Key(this, name, display, duration, loseInvChance, breakChance, onlyEmpty);
            keys.add(key);
        }
    }

    /*
    Gets the correct using the configname
    @param configName - The config name of the key
     */
    public Key getKey(String configName){
        for(Key key : keys){
            if(configName.equals(key.getConfigName())){
                return key;
            }
        }
        return null;
    }

    /*
    Gets the correct corresponding key using the key itemstack.
    @param stack - The itemstack to be compared.
     */
    public Key getKey(ItemStack stack){
        for(Key key : keys){
            if(ItemUtil.getTag(stack, "lwckey") == null){
                return null;
            }

            if(ItemUtil.getTag(stack, "lwckey").toString().equals(key.getConfigName())){
                return key;
            }
        }

        return null;
        /*
        for(Key key : keys){
            if(key.createKey().isSimilar(stack)){
                return key;
            }
        }
        return null;*/
    }

}
